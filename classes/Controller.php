<?php
abstract class Controller{
	protected $request ='';
	protected $action  ='';
	protected $id      ='';

	public function __construct($action, $request){
		$this->action = $action;
		$this->request = $request;
		$this->id = $request['id'];
	}

	public function executeAction(){
		$id = $this->id;
		return $this->{$this->action}($id);
	}

	protected function returnView($viewmodel, $fullview){
		$view = 'views/'. get_class($this). '/' . $this->action. '.php';
		if($fullview){
			require('views/main.php');
		} else {
			require($view);
		}
	}
}